from torch.utils.data.dataset import Dataset
import torchvision
from torchvision.io import read_image
from PIL import Image
import os

import abc
from collections import namedtuple


FairnessSample = namedtuple('FairnessSample', ['path', 'prot_label', 'target_label'])

class FairnessDataset(Dataset, metaclass=abc.ABCMeta):
    def __init__(
        self,
        *args,
        **kwargs,
    ):

        self.data_dir = kwargs.get('data_dir', '')
        self.prot_attr = kwargs.get('prot_attr', '')
        self.target_attr = kwargs.get('target_attr', '')

        self.split = kwargs.get('split', 'all')

        self.samples = list() # List(FairnessSample)
        self.prot_labels = set()
        self.target_labels = set()

        # If labels are initially provided as strings these are dicts that convert them to ints
        self.prot_2_int = None
        self.target_2_int = None


        self._load_samples()

    def _add_sample(self, path: str, prot_label: int, target_label: int) -> None:
        self.samples.append(
            FairnessSample(
                path=path,
                prot_label=prot_label,
                target_label=target_label
        ))
        self.prot_labels.add(prot_label)
        self.target_labels.add(target_label)

    @abc.abstractmethod
    def _load_samples(self, *args, **kwargs):
        pass

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        # img = read_image(os.path.join(self.data_dir, self.samples[idx].path))
        img = Image.open(os.path.join(self.data_dir, self.samples[idx].path))
        return img, self.samples[idx].prot_label, self.samples[idx].target_label