from random import random
from threading import local
import torch
import torchvision.transforms as transforms
import torchvision
from torch.utils.data.dataset import Subset, ConcatDataset, Dataset

import os
from PIL import ImageFilter, ImageOps, Image
import numpy as np
import csv
import abc
from collections import namedtuple

from typing import Any, Callable, cast, Dict, List, Optional, Tuple

from .bupt import BuptBalancedFace
from .celeba import CelebA
from .fairface import FairFace
from .utkface import UTKFace
from .marco import MarcoC3
from .utils import *
from .augmentation import DataAugmentationMultiCrop
from .sampler import BalancedSubsetRandomSampler, RandomSampler, StatisticalSubsetRandomSampler, RandomSamplerDist




class DataloaderFactory(object):
    def __init__(self,
        distributed=False,
        num_workers=4,
    ):  
        self.dist = distributed
        self.num_workers = num_workers

        self.samplers = list()

    def _create_dataloader(self, dataset, batch_size, shuffle, dist):
        sampler = None
        if dist is None or dist:
            if self.dist:
                sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=shuffle)
                self.samplers.append(sampler)
            shuffle = not self.dist and shuffle

        dataloader = torch.utils.data.DataLoader(
            dataset,
            batch_size=batch_size,
            shuffle=shuffle,
            num_workers=self.num_workers,
            sampler=sampler,
            pin_memory=True
        )
        return dataloader



    def dataloader(self, dataset, batch_size, shuffle=True, dist=None):
        return self._create_dataloader(
            dataset,
            batch_size,
            shuffle,
            dist,
        )

    def _create_iteration_dataloader(self, dataset, batch_size, iterations, balance=None, bias_dist=None, sampler_seed=0):
        sampler = None
        if balance == 'sampler':
            sampler =  BalancedSubsetRandomSampler(
                create_joint_labels(get_dataset_samples(dataset)),
                num_samples = iterations * batch_size,
                num_replicas=None if self.dist else 1,
                rank=None if self.dist else 0,
                seed=sampler_seed,
            )
        elif balance == 'stat_sampler':
            sampler =  StatisticalSubsetRandomSampler(
                bias_dist = bias_dist,
                bias_labels = [x.prot_label for x in get_dataset_samples(dataset)],
                target_labels = [x.target_label for x in get_dataset_samples(dataset)],
                num_samples = iterations * batch_size,
                num_replicas=None if self.dist else 1,
                rank=None if self.dist else 0,
                seed=sampler_seed,
            )
        else:
            if self.dist:
                sampler = RandomSamplerDist(
                    indices = [x for x in range(len(dataset))],
                    num_samples = iterations * batch_size,
                    num_replicas=None if self.dist else 1,
                    rank=None if self.dist else 0,
                    seed=sampler_seed,
                )
            else:
                sampler = RandomSampler(
                    indices = [x for x in range(len(dataset))],
                    num_samples = iterations * batch_size,
                )

        if sampler is not None:
            self.samplers.append(sampler)

        dataloader = torch.utils.data.DataLoader(
            dataset,
            batch_size=batch_size,
            num_workers=self.num_workers,
            sampler=sampler,
            pin_memory=True
        )
        return dataloader

    def iteration_random_dataloader(self, dataset, batch_size, iterations, balance=None, bias_dist=None, sampler_seed=0):
        return self._create_iteration_dataloader(
            dataset,
            batch_size,
            iterations,
            balance=balance,
            bias_dist=bias_dist,
            sampler_seed=sampler_seed,
        )

    def set_epoch(self, epoch):
        if self.dist:
            for sampler in self.samplers:
                sampler.set_epoch(epoch)

    def update_sampler_bias_dist(self, bias_dist):
        for sampler in self.samplers:
            if isinstance(sampler, StatisticalSubsetRandomSampler):
                sampler.update_bias_dist(bias_dist)


class DatasetFactory(object):
    """
    Class that can be used to create different kinds of fairness datasets.
    Contains the necessary methods for splitting the data into train/valid/test splits.
    """
    def __init__(
        self,
        data_dir='',
        dataset='FAIRFACE',
        prot_attr=None,
        target_attr=None,
        val_test_pct=0.1,
        val_test_count=None,
        val_set=True,
        seed=0,
        shuffle=True,
        match_val_test_size=True,
        balance_train_with_dupes=False,
        randomize_splits=True,
        train_skew=None,
        train_corr=None,
        train_bias_classes='all',
        data_aug='all',
        local_crops=4,
        img_size=224,
        *args,
        **kwargs,
    ):
        # Create the dataset
        self.dataset = dataset
        self.data_dir = data_dir
        
        self.prot_attr = prot_attr
        self.target_attr = target_attr
        self.val_test_pct = val_test_pct
        self.val_test_count = val_test_count
        self.val_set = val_set
        self.match_val_test_size = match_val_test_size
        self.randomize_splits = randomize_splits
        self.train_bias_classes = train_bias_classes

        self.data_aug = data_aug
        self.local_crops = local_crops
        self.img_size = img_size

        self.args = args
        self.kwargs = kwargs

        self.train_skew = train_skew
        self.train_corr = train_corr
        # Skew and corr can not be used together
        assert train_corr is None or train_skew is None
       

        # Use a generator with a set seed so the classes are split the same way each time
        self.rng = torch.Generator()
        self.rng.manual_seed(seed)
        self.shuffle = shuffle
        self.balance_train_with_dupes = balance_train_with_dupes

        self._all_dataset = None
        self._train_dataset = None
        self._valid_dataset = None
        self._test_dataset = None

        # This may lead to a memory leak, might want to hand off the samplers
        self.samplers = []

        self._init_datasets()
        self._init_transformations()

    
    def _get_random_splits(
        self,
    ):  
        val = None
        # Split each (pro_attr, target_attr) pair evenly across each partition
        datasets = \
            self._split_dataset_evenclasses(
                self._base_dataset,
                create_joint_labels(get_dataset_samples(self._base_dataset)),
                split_pct=self.val_test_pct,
                split_count=self.val_test_count,
                num_splits=2 if self.val_set else 1,
            )
        if self.val_set:
            train, val, test = datasets
        else:
            train, test = datasets

        return train, val, test

    def _init_datasets(
        self,
    ):  
        # Create the datasets
        if self.dataset == 'CELEBA':
            dataset_class = CelebA
        elif self.dataset == 'FAIRFACE':
            dataset_class = FairFace
        elif self.dataset == 'BUPT_BALANCED':
            dataset_class = BuptBalancedFace
        elif self.dataset == 'UTKFACE':
            dataset_class = UTKFace
        elif self.dataset == 'MARCOC3':
            dataset_class = MarcoC3
        else:
            raise Exception(f'Dataset {self.dataset} is not recognized.')
        
        self._base_dataset = dataset_class(
                data_dir=self.data_dir, 
                prot_attr=self.prot_attr,
                target_attr=self.target_attr,
                split='all',
                *self.args, **self.kwargs
            )

        assert len(self._base_dataset) > 0
        
        if self.randomize_splits:
            train, val, test = self._get_random_splits()
        else:
            train = dataset_class(
                data_dir=self.data_dir, 
                prot_attr=self.prot_attr,
                target_attr=self.target_attr,
                split='train',
                *self.args, **self.kwargs
            )
            test = dataset_class(
                data_dir=self.data_dir, 
                prot_attr=self.prot_attr,
                target_attr=self.target_attr,
                split='test',
                *self.args, **self.kwargs
            )
            if self.val_set:
                val = dataset_class(
                    data_dir=self.data_dir, 
                    prot_attr=self.prot_attr,
                    target_attr=self.target_attr,
                    split='valid',
                    *self.args, **self.kwargs
                )
            else:
                val = test
        

        if val is not None and self.match_val_test_size:
            if len(test) > len(val):
                test = self._trim_dataset(test, len(val))
            elif len(val) > len(test):
                val = self._trim_dataset(val, len(test))

        if self.train_skew is not None:
            train = self._trim_dataset(train)
            train = self._adjust_skew(train, self.train_skew)
            
        if self.train_corr is not None:
            train = self._trim_dataset(train)
            train = self._adjust_corr(train, self.train_corr)


        if self.train_bias_classes != 'all':
            train = self._subsample_bias(train, [int(x) for x in self.train_bias_classes])


        if self.balance_train_with_dupes:
            train = self._balance_dataset(train)

        self._train_dataset = train
        if self.val_set:
            self._valid_dataset = val
        else:
            self._valid_dataset = test
            
        self._test_dataset = test
        self._all_dataset = self._base_dataset

    def _init_transformations(self):
        # TODO pass mean and std
        if self.data_aug == 'all':
            self.train_transforms = transforms.Compose([
                transforms.ToTensor(),
                transforms.RandomResizedCrop(
                    self.img_size,
                    antialias=True,
                ),
                transforms.RandomApply([
                    transforms.ColorJitter(0.4, 0.4, 0.4, 0)  # not strengthened
                ], p=0.8),
                transforms.RandomHorizontalFlip(),
                # transforms.ConvertImageDtype(torch.float),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ])
        elif self.data_aug == 'none':
            self.train_transforms = self.test_transforms
        else:
            raise Exception(f'{self.data_aug} is not inplemented.')

        self.test_transforms = transforms.Compose([
            transforms.ToTensor(),
            transforms.Resize(self.img_size, antialias=True),
            transforms.CenterCrop(self.img_size),
            # transforms.ConvertImageDtype(torch.float),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])

    def _get_transforms(self, transforms):
        if transforms == 'test':
            transforms = self.test_transforms
        elif transforms == 'train':
            transforms = self.train_transforms
        elif transforms == 'valid':
            transforms = self.test_transforms
        return transforms



###############################################################################
# Dataset modification functions
###############################################################################

    def _subsample_bias(
        self,
        dataset,
        bias_classes,
    ):
        """
        Takes a dataset and a list of integer bias classes
        Returns: A new dataset that only has samples from dataset which are
            the supplied bias classes
        """
        bias_labels = torch.tensor([x.prot_label for x in get_dataset_samples(dataset)])
        mask = torch.zeros_like(bias_labels).bool()
        for bias_class in bias_classes:
            mask = torch.logical_or(mask, bias_labels == bias_class)
        
        return Subset(dataset,torch.arange(bias_labels.shape[0])[mask])


    def _balance_dataset(
        self,
        dataset,
    ):
        '''
        Takes a dataset and balances the (protected, target) pairs by oversampling
        '''
        joint_labels = torch.tensor(create_joint_labels(get_dataset_samples(dataset)))
        joint_classes, counts = joint_labels.unique(return_counts=True)

        balance_target = counts.max()

        joint_idx = torch.arange(joint_labels.shape[0])

        superset_idxs = []

        for jc in joint_classes:
            mask = jc == joint_labels

            if self.shuffle:
                rand_perm = torch.randperm(mask.sum(), generator=self.rng)
            else:
                rand_perm = torch.arange(mask.sum())
            idxs = joint_idx[mask][rand_perm]

            while idxs.shape[0] < balance_target:
                idxs = torch.cat([idxs, idxs])

            idxs = idxs[:balance_target]

            superset_idxs.append(idxs)

        superset_idxs = torch.cat(superset_idxs)
    
        return Subset(dataset, superset_idxs)

    def _adjust_skew(
        self,
        dataset,
        skew,
    ):
        """
        Takes a dataset and adjusts the skew across the (protected, pairs)
        """
        samples = get_dataset_samples(dataset)
        target_labels = torch.tensor([x.target_label for x in samples])
        prot_labels = torch.tensor([x.prot_label for x in samples])

        target_classes = target_labels.unique()
        prot_classes = prot_labels.unique()

        num_target_classes = len(target_classes)
        num_prot_classes = len(prot_classes)

        # a = list(np.linspace(1, 1-skew, num=num_target_classes))
        # b = list(reversed(a))

        a = list(np.linspace(1, 1-skew, num=num_prot_classes))
        b = list(a)

        a = torch.tensor(a)
        b = torch.tensor(b)

        label_dist = torch.nn.functional.interpolate(
            input = torch.stack([a,b]).unsqueeze(0).unsqueeze(0),
            size = (num_target_classes, num_prot_classes),
            mode = 'bilinear',
            align_corners = True,
        ).squeeze().transpose(1, 0)


        subset_idxs = []

        sample_idxs = torch.arange(len(samples))
        for i, prot_class in enumerate(prot_classes):
            prot_mask = prot_labels == prot_class
            for j, target_class in enumerate(target_classes):
                target_mask = target_labels == target_class

                mask = torch.logical_and(prot_mask, target_mask)
                count = (mask.sum() * label_dist[i][j]).round().int()

                subset_idxs.append(sample_idxs[mask][:count])


        subset_idxs = torch.cat(subset_idxs, dim=0)

        return Subset(dataset, subset_idxs)

    def _adjust_corr(
        self,
        dataset,
        corr,
    ):
        """
        Takes a dataset and adjusts the correlation across the (protected, pairs)
        """
        samples = get_dataset_samples(dataset)
        target_labels = torch.tensor([x.target_label for x in samples])
        prot_labels = torch.tensor([x.prot_label for x in samples])

        target_classes = target_labels.unique()
        prot_classes = prot_labels.unique()

        num_target_classes = len(target_classes)
        num_prot_classes = len(prot_classes)

        a = list(np.linspace(1, 1-corr, num=num_target_classes))
        b = list(reversed(a))

        a = torch.tensor(a)
        b = torch.tensor(b)

        label_dist = torch.nn.functional.interpolate(
            input = torch.stack([a,b]).unsqueeze(0).unsqueeze(0),
            size = (num_prot_classes, num_target_classes),
            mode = 'bilinear',
            align_corners = True,
        ).squeeze()

        subset_idxs = []

        sample_idxs = torch.arange(len(samples))
        for i, prot_class in enumerate(prot_classes):
            prot_mask = prot_labels == prot_class
            for j, target_class in enumerate(target_classes):
                target_mask = target_labels == target_class

                mask = torch.logical_and(prot_mask, target_mask)
                count = (mask.sum() * label_dist[i][j]).round().int()

                subset_idxs.append(sample_idxs[mask][:count])


        subset_idxs = torch.cat(subset_idxs, dim=0)

        return Subset(dataset, subset_idxs)
        


    def _trim_dataset(
        self,
        dataset,
        new_size=None,
    ):
        """
        Takes a dataset a returns a joint label balanced dataset which is now new_size
        If new_size is None then maximize the dataset size
        """

        joint_labels = torch.tensor(create_joint_labels(get_dataset_samples(dataset)))

        joint_classes, counts = joint_labels.unique(return_counts=True)

        if new_size is None:
            # Find the maximum dataset size
            min_size = np.inf
            for joint_class in joint_classes:
                class_size = joint_labels
            subset_cnt = counts.min()
        else:
            subset_cnt = new_size // joint_classes.shape[0]

        joint_idx = torch.arange(joint_labels.shape[0])
        
        subset_idxs = []
        for jc in joint_classes:
            mask = joint_labels == jc
            subset_idxs.append(joint_idx[mask][:subset_cnt])

        subset_idxs = torch.cat(subset_idxs, dim=0)
        
        return Subset(dataset, subset_idxs)


    def _split_dataset_evenclasses(
        self,
        dataset,
        labels,
        num_splits=2,
        split_pct=0.1,
        split_count=None
    ):
        """
        Takes a dataset and splits it into three new datasets which are train, validation and test
        partitions. 
        Makes sure the classes are balanced evenly between the three splits

        split (float): percentage of the data to include in the split partition

        split_count (int): the number of samples to include in the split partition

        """

        # Only one of val_test_split or val_test_count must be None
        assert (split_count is None and split_pct is not None) or \
            (split_count is not None and split_pct is None)

        split_datasets = []

        classes, inverse_idx = np.unique(labels, return_inverse=True, return_index=False)

        base_idxs = []
        splits_idxs = [[] for x in range(num_splits)]

        sample_idxs = torch.arange(len(labels)) 
        # Classes must be integers from [0,classes.shape[0])
        for i in range(classes.shape[0]):
            class_idxs = sample_idxs[inverse_idx==i]
            if split_count is None:
                split_point = int(round(class_idxs.shape[0] * split_pct)) 
            else:
                split_point = split_count
            if self.shuffle:
                rand_perm = torch.randperm(class_idxs.shape[0], generator=self.rng)
            else:
                rand_perm = torch.arange(class_idxs.shape[0])

            for j in range(num_splits):
                splits_idxs[j].append(
                    class_idxs[rand_perm[j*split_point:(j+1)*split_point]]
                )
            base_idxs.append(class_idxs[rand_perm[(j+1)*split_point:]])

        for j in range(num_splits):
            sorted_indices, _ = torch.sort(torch.cat(splits_idxs[j], dim=0))
            split_datasets.append(Subset(dataset, sorted_indices))

        sorted_indices, _ = torch.sort(torch.cat(base_idxs, dim=0))
        return [Subset(dataset, sorted_indices)] + split_datasets


###############################################################################
# Dataset loading functions
###############################################################################

    def get_dataset_bias_classes(self, dataset):
        return len(set([int(x.prot_label) for x in get_dataset_samples(dataset)]))

    def get_train_bias_classes(self):
        return self.get_dataset_bias_classes(self._train_dataset)

    def get_val_bias_classes(self):
        return self.get_dataset_bias_classes(self._valid_dataset)

    def get_test_bias_classes(self):
        return self.get_dataset_bias_classes(self._test_dataset)    
    
    def get_total_bias_classes(self):
        return self.get_dataset_bias_classes(self._base_dataset)

    def get_dataset_target_classes(self, dataset):
        return len(set([int(x.target_label) for x in get_dataset_samples(dataset)]))

    def get_train_target_classes(self):
        return self.get_dataset_target_classes(self._train_dataset)

    def get_val_target_classes(self):
        return self.get_dataset_target_classes(self._valid_dataset)

    def get_test_target_classes(self):
        return self.get_dataset_target_classes(self._test_dataset)   
    
    def get_total_target_classes(self):
        return self.get_dataset_target_classes(self._base_dataset)

    def get_train_len(self):
        return len(self._train_dataset)

    def get_valid_len(self):
        return len(self._valid_dataset)

    def get_test_len(self):
        return len(self._test_dataset)

    def get_dataset(self, split, transform='split'):
        if transform == 'split':
            transform = split
        if split == 'train':
            dataset = self._train_dataset
        elif split == 'valid':
            dataset = self._valid_dataset
        elif split == 'test':
            dataset = self._test_dataset
        elif split == 'all':
            dataset = self._all_dataset
        else:
            raise Exception('Dataset split not supported.')
        if transform is not None:
            dataset = TransformWrapper(dataset, self._get_transforms(transform))

        return dataset

    

