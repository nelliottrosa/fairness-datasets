import torch
from torch.utils.data.dataset import Dataset
from torch.nn import Identity
from torch.utils.data.dataset import Subset

from .generic import FairnessDataset

####################################################################################################
# Augmentation and transform classes
####################################################################################################
class TransformWrapper(Dataset):
    def __init__(self,
        dataset,
        transforms,
        pro_target_transforms=None,
        attr_target_transforms=None
    ):
        self.transforms = transforms
        self.pro_target_transforms = pro_target_transforms
        self.attr_target_transforms = attr_target_transforms
        self.dataset = dataset

        if self.pro_target_transforms is None:
            self.pro_target_transforms = Identity()
        if self.attr_target_transforms is None:
            self.attr_target_transforms = Identity()

    def __getitem__(self, idx):
        if self.dataset is None:
            return None
        sample = self.dataset[idx]
        img = sample[0]
        prot_label = sample[1]
        target_label = sample[2]

        ret_lst = (
            self.transforms(img),
            self.pro_target_transforms(prot_label),
            self.attr_target_transforms(target_label),
        )
        if len(sample) > 3:
            ret_lst = ret_lst + sample[3:]
        return ret_lst
            
            

    def __len__(self):
        return len(self.dataset) if not self.dataset is None else 0


def get_class_balance(dataset, normalize=True, orig_name=True):
    '''
    Takes a fairness dataset and returns the normalized class balance of (protected, target) pairs

    orig_name (bool): If true returns the labels as the orginal strings rather than ints
    '''
    ds_samples = get_dataset_samples(dataset)
    joint_labels, label_pairs = create_joint_labels(ds_samples, return_pairs=True)
    joint_labels = torch.tensor(joint_labels)
    joint_classes, counts = joint_labels.unique(return_counts=True)

    # Create inverse
    prot_2_int = get_base_dataset(dataset).prot_2_int
    if orig_name and prot_2_int is not None:
        int_2_prot_dict = dict()
        for key in prot_2_int:
            int_2_prot_dict[    prot_2_int[key]] = key
            def int_2_prot(int_label):
                return int_2_prot_dict[int_label]
    else:
        def int_2_prot(int_label):
            return int_label

    target_2_int = get_base_dataset(dataset).target_2_int
    if orig_name and target_2_int is not None:
        int_2_target_dict = dict()
        for key in target_2_int:
            int_2_target_dict[target_2_int[key]] = key
            def int_2_target(int_label):
                return int_2_target_dict[int_label]
    else:
        def int_2_target(int_label):
            return int_label

    label_pairs = [(int_2_prot(x[0]), int_2_target(x[1])) for x in label_pairs]


    if normalize:
        counts = counts.float() / counts.sum()
 
    return [x.item() for x in counts], label_pairs



def get_dataset_samples(dataset):
    '''
    Recursively moves through subset datasets to get the labels from the base fairness dataset
    '''
    
    if issubclass(type(dataset), FairnessDataset):
        return dataset.samples
    elif isinstance(dataset, Subset):
        labels = get_dataset_samples(dataset.dataset)
        return [labels[x] for x in dataset.indices]
    elif isinstance(dataset, TransformWrapper):
        return get_dataset_samples(dataset.dataset)
    else:
        raise Exception("Dataset type not recognized.")


def create_joint_labels(samples, return_pairs=False):
    '''
    Takes a set of fairness samples and returns integer labels where 
    each unique (target, prot) label pair is represented by a unique integer
    '''
    num_target_classes = len(set([x.target_label for x in samples]))
    joint_labels = [
        x.prot_label*num_target_classes + x.target_label
            for x in samples
    ]

    if return_pairs:
        joint_classes = set(joint_labels)
        pairs = [(x // num_target_classes, x % num_target_classes) for x in joint_classes]
        return joint_labels, pairs

    return joint_labels

def get_base_dataset(dataset):
    '''
    Recursively searches through split datasets to find the original dataset
    '''
    if issubclass(type(dataset), FairnessDataset):
        return dataset
    elif isinstance(dataset, Subset):
        return get_base_dataset(dataset.dataset)
    elif isinstance(dataset, TransformWrapper):
        return get_base_dataset(dataset.dataset)
    else:
        raise Exception("Dataset type not recognized.")
