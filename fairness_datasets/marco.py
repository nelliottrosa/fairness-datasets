import os
import csv

from natsort import natsorted

from .generic import FairnessDataset

class MarcoC3(FairnessDataset):

    def __init__(self,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)


    def _load_label_csv(self, filename):
        with open(os.path.join(self.data_dir, filename), 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            # Skip the header line
            next(csvreader)
            return list(csvreader)

    def _load_samples(self, *args, **kwargs):

        test_samples = self._load_label_csv('test-info.csv') + \
            self._load_label_csv('c3-info.csv')
        
        train_samples = self._load_label_csv('train-info.csv')

        if self.split == 'all':
            samples = test_samples + train_samples
        elif self.split == 'test':
            samples = test_samples
        elif self.split == 'train':
            samples = train_samples
        elif self.split == 'valid':
            raise NotImplementedError()


        samples = [
            (
                x[0],
                int(self._get_label_from_sample(x, self.prot_attr)),
                int(self._get_label_from_sample(x, self.target_attr)),
            )
            for x in samples
        ]




        for path, prot_label, target_label in samples:
            self._add_sample(
                path = path,
                prot_label = prot_label,
                target_label = target_label
            )

    def _get_label_from_sample(self, sample, attr):
        if len(sample) == 6:
            sample = [sample[0], sample[2], sample[5]]
        map_dict = {
            'label': 1,
            'source': 2,
        }
        return sample[map_dict[attr]]