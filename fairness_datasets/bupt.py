from .generic import FairnessDataset

from typing import Tuple, List, Dict
import os

class BuptBalancedFace(FairnessDataset):
    '''
    attr names can be race or ID
    '''

    def __init__(self,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)


    def _find_classes(self, data_dir: str) -> Tuple[List[str], List[str], Dict[str, int], Dict[str, int]]:
        '''
            Classes are sorted in dir as race/id/images
        '''
        race_classes = sorted(entry.name for entry in os.scandir(data_dir) if entry.is_dir())
        id_classes = sorted([entry.name for x in race_classes for entry in os.scandir(os.path.join(data_dir, x))])

        race_class_to_idx = {cls_name: i for i, cls_name in enumerate(race_classes)}
        id_class_to_idx = {cls_name: i for i, cls_name in enumerate(id_classes)}

        return race_classes, id_classes, race_class_to_idx, id_class_to_idx


    def _load_samples(self, *args, **kwargs):
        self.attr_map = {
            'race':1,
            'id':2
        }

        assert(self.target_attr in self.attr_map.keys())
        assert(self.prot_attr in self.attr_map.keys())

        self.race_classes, self.id_classes, self.race_class_to_idx, self.id_class_to_idx = \
            self._find_classes(self.data_dir)

        samples = self._make_dataset(
            self.data_dir,
            self.race_classes,
            self.id_classes,
            self.race_class_to_idx,
            self.id_class_to_idx
        )

        for sample in samples:
            self._add_sample(
                sample[0],
                sample[self.attr_map[self.prot_attr]],
                sample[self.attr_map[self.target_attr]]
            )
            


    def _make_dataset(self,
            data_dir: str,
            race_classes: List[str],
            id_classes: List[str],
            race_class_to_idx: Dict[str, int],
            id_class_to_idx: Dict[str, int],
        ) -> List[Tuple[str, int, int]]:
        '''
        Generates a list of samples
        '''
        data_dir = os.path.expanduser(data_dir)


        instances = []
        for race_class in sorted(race_class_to_idx.keys()):
            for id_class in sorted(id_class_to_idx.keys()):
                race_index = race_class_to_idx[race_class]
                id_index = id_class_to_idx[id_class]
                target_dir = os.path.join(data_dir, race_class, id_class)

                if not os.path.isdir(target_dir):
                    continue

       
                for root, _, fnames in sorted(os.walk(target_dir, followlinks=True)):
                    for fname in sorted(fnames):
                        path = os.path.join(root, fname)
                        item = path, race_index, id_index
                        instances.append(item)


        return instances



    

    # def __len__(self):
    #     return len(self.samples)

    # def __getitem__(self, idx):
    #     path = self.samples[idx][0]
    #     attrs = self.samples[idx][1:]
    #     pro_target = attrs[self.attr_map[self.pro_attr]]
    #     attr_target = attrs[self.attr_map[self.target_attr]]
    #     sample = torchvision.datasets.folder.default_loader(os.path.join(self.data_dir, path))
    #     return sample, pro_target, attr_target, path
