import os, random

from .generic import FairnessDataset

from natsort import natsorted

class UTKFace(FairnessDataset):
    '''
    attr names can be race or ID
    '''

    def __init__(self,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)

    def _load_samples(self, *args, **kwargs):
        attr_map = {
            'age':0,
            'gender':1,
            'race':2
        }

        def age_map(age):
            age = int(age)
            if age < 20:
                return 0
            elif age < 40:
                return 1
            else:
                return 2



        filenames =  natsorted(entry.name for entry in os.scandir(self.data_dir))
        
        for path in filenames:
            sample_labels = path.split('_')[:-1]
            if len(sample_labels) != 3:
                continue
            try:
                sample_labels = (
                    age_map(sample_labels[0]),
                    int(sample_labels[1]),
                    self._convert_race_label(int(sample_labels[2]))
                )
            except ValueError:
                continue
            
            # Skip other race labels
            if sample_labels[attr_map['race']] != 4:
                self._add_sample(
                    path = path,
                    prot_label = sample_labels[attr_map[self.prot_attr]],
                    target_label = sample_labels[attr_map[self.target_attr]]
                )

        # Same order as used in the MFD paper https://arxiv.org/abs/2106.04411
        random.Random(1).shuffle(self.samples)


    def _convert_race_label(self, label):
        """ Takes a UTKFace label and converts it to the integer conventions of BUPT balanced
        African: 1. Asian: 2, Cauc: 0, Indian: 3 -> African: 0, Asian: 1, Cauc: 2, Indian: 3
        """
        label_map = [2, 0, 1, 3, 4]
        return label_map[label]
        