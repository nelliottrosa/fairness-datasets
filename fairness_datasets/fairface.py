import os
import csv

from natsort import natsorted

from .generic import FairnessDataset

class FairFace(FairnessDataset):

    def __init__(self,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)


    def _load_label_csv(self, filename):
        with open(os.path.join(self.data_dir, filename), 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            # Skip the header line
            next(csvreader)
            return list(csvreader)

    def _load_samples(self, *args, **kwargs):
        self.prot_2_int = dict()
        self.target_2_int = dict()

        samples = self._load_label_csv('fairface_label_train.csv') + \
           self._load_label_csv('fairface_label_val.csv')

        samples = [
            (
                x[0],
                self._get_label_from_sample(x, self.prot_attr),
                self._get_label_from_sample(x, self.target_attr)
            )
            for x in samples
        ]

        prot_classes =set([x[1] for x in samples])
        target_classes = set([x[2] for x in samples])

        for i, prot_class in enumerate(natsorted(prot_classes)):
            self.prot_2_int[prot_class] = i

        for i, target_class in enumerate(natsorted(target_classes)):
            self.target_2_int[target_class] = i



        for path, prot_label, target_label in samples:
            self._add_sample(
                path = path,
                prot_label = self.prot_2_int[prot_label],
                target_label = self.target_2_int[target_label]
            )

    def _get_label_from_sample(self, sample, attr):
        if attr == 'age':
            age_map = {
                '0-2':'0-19',
                '3-9':'0-19',
                '10-19':'0-19',
                '20-29':'20-29',
                '30-39':'30-39',
                '40-49':'40+',
                '50-59':'40+',
                '60-69':'40+',
                'more than 70':'40+',
            }
            return age_map[sample[1]]
        if attr == 'gender':
            return sample[2]
        if attr == 'race':
            return sample[3]