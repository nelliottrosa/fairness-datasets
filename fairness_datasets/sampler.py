import torch
from torch.utils.data.sampler import Sampler

import torch.distributed as dist

from typing import Iterator, Iterable, Optional, Sequence, List, TypeVar, Generic, Sized, Union
import math

def get_rank_and_replica(rank, num_replicas):
    if num_replicas is None:
        if not dist.is_available():
            raise RuntimeError("Requires distributed package to be available")
        num_replicas = dist.get_world_size()
    if rank is None:
        if not dist.is_available():
            raise RuntimeError("Requires distributed package to be available")
        rank = dist.get_rank()
    if rank >= num_replicas or rank < 0:
        raise ValueError(
            "Invalid rank {}, rank should be in the interval"
            " [0, {}]".format(rank, num_replicas - 1))
    
    return rank, num_replicas

class CycleRandomPermutation(object):
    def __init__(self, cycle_len: int):
        self.cycle_len = cycle_len

    def __iter__(self) -> Iterator[int]:
        while True:
            for i in torch.randperm(self.cycle_len):
                yield i

class CycleRandomPermutationV2(object):
    '''
    Given a list of idxs yields the idxs in a random order. Repeats with a 
    different random order once the end of the list is reached
    '''
    def __init__(self,
        idxs: List[int],
        generator=None,
        num_replicas: Optional[int] = None,
        rank: Optional[int] = None,
    ):
        self.idxs = idxs
        assert len(self.idxs) > 0

        if generator is None:
            self.g = torch.Generator()
        else:
            self.g = generator

        self.rank, self.num_replicas = get_rank_and_replica(rank=rank, num_replicas=num_replicas)

    def __iter__(self) -> Iterator[int]:
        count = 0
        while True:
            for i in torch.randperm(len(self.idxs), generator=self.g):
                count += 1
                if self.rank is not None and self.num_replicas is not None:
                    if (count + self.rank) % self.num_replicas == 0:
                        yield self.idxs[i]
                    else:
                        continue
                    

class MultiCycleRandomPermutation(object):
    def __init__(self,
        idxs_list: List[List[int]],
        num_replicas: Optional[int] = None,
        rank: Optional[int] = None,
    ):
        self.idxs_list_cyclers = [CycleRandomPermutationV2(
            x,
            num_replicas=num_replicas,
            rank=rank,
        ) for x in idxs_list]

    def __iter__(self) -> Iterator[int]:
        iters = [iter(x) for x in self.idxs_list_cyclers]
        while True:
            for cycler in iters:
                yield next(cycler)




class StatisticalSubsetRandomSampler(Sampler[int]):
    # r"""Samples elements randomly from a given list of indices, without replacement.

    # Args:
    #     indices (sequence): a sequence of indices
    #     generator (Generator): Generator used in sampling.
    # """
    indices: Sequence[int]

    def __init__(self,
        bias_dist,
        bias_labels,
        target_labels,
        num_samples=None,
        num_replicas: Optional[int] = None,
        rank: Optional[int] = None,
    ) -> None:
        self.num_samples = num_samples if num_samples is not None else len(bias_labels)
        self.bias_dist = bias_dist

        self.num_bias_classes = len(set(bias_labels))
        num_target_classes = len(set(target_labels))

        assert len(target_labels) == len(bias_labels)
        self._check_bias_dist(bias_dist)
        
        idx_lists = [[[] for _ in range(num_target_classes)] for _ in range(self.num_bias_classes) ]
        # Calculate the bias subset indices
        for i in range(len(bias_labels)):
            idx_lists[bias_labels[i]][target_labels[i]].append(i)

        self.cyclers = [MultiCycleRandomPermutation(
            idx_lists[i],
            num_replicas=num_replicas,
            rank=rank,
        ) for i in range(self.num_bias_classes)]

    def __iter__(self) -> Iterator[int]:
        # Sample from the cyclers according to bias_dist
        iters = [iter(x) for x in self.cyclers]
        while True:
            rand_num = torch.rand(1)
            running_sum = 0
            for i in range(len(self.bias_dist)):
                running_sum = running_sum + self.bias_dist[i]
                if rand_num < running_sum:
                    yield next(iters[i])
                    break

    def __len__(self) -> int:
        return self.num_samples
    
    def update_bias_dist(self, bias_dist):
        self._check_bias_dist(bias_dist)
        self.bias_dist = bias_dist

    def _check_bias_dist(self, bias_dist):
        dist_sum = sum(bias_dist)
        assert len(bias_dist) == self.num_bias_classes
        assert math.isclose(dist_sum, 1., rel_tol=1e-3)


class BalancedSubsetRandomSampler(Sampler[int]):
    r"""Samples elements randomly from a given list of indices, without replacement.

    Args:
        
    """
    indices: Sequence[int]

    def __init__(self,
        labels,
        num_samples=None,
        seed=0,
        num_replicas: Optional[int] = None,
        rank: Optional[int] = None,
    ) -> None:
        
        self.num_samples = num_samples if num_samples is not None else len(labels)
        self.labels = labels

        # Calculate the bias subset indices
        self.label_subsets = []
        for klass in set(self.labels):
           indices = [i for i, x in enumerate(self.labels) if x == klass]
           self.label_subsets.append(indices)

        # Used for random seeds across multiple GPU instances
        self.epoch = 0
        self.seed = seed

        self.rank, self.num_replicas = get_rank_and_replica(rank=rank, num_replicas=num_replicas)

        

    def __iter__(self) -> Iterator[int]:
        g = torch.Generator()
        g.manual_seed(self.seed + self.epoch)  

        label_subsets = [
            iter(CycleRandomPermutationV2(
                x,
                rank=self.rank,
                num_replicas=self.num_replicas,
                generator=g,
            )) 
            for x in self.label_subsets
        ]
        for i in range(self.num_samples):
            idx = i % len(label_subsets)
            yield next(label_subsets[idx])

    def __len__(self) -> int:
        return self.num_samples
    
    def set_epoch(self, epoch: int) -> None:
        r"""
        Sets the epoch for this sampler. When :attr:`shuffle=True`, this ensures all replicas
        use a different random ordering for each epoch. Otherwise, the next iteration of this
        sampler will yield the same ordering.
        Args:
            epoch (int): Epoch number.
        """
        self.epoch = epoch
    

# TODO test this class
class RandomSampler(Sampler[int]):
    def __init__(self, indices, num_samples=None):
        self.num_samples = num_samples if num_samples is not None else len(indices)
        self.indices = indices
        self.indices_randperm = iter(CycleRandomPermutation(len(indices)))

    def __iter__(self) -> Iterator[int]:
        for _ in range(self.num_samples):
            for i in self.indices_randperm:
                yield self.indices[i]

    def __len__(self) -> int:
        return self.num_samples
    
class RandomSamplerDist(Sampler[int]):
    def __init__(self,
        indices,
        num_samples=None,
        seed=0,
        num_replicas: Optional[int] = None,
        rank: Optional[int] = None,
    ):
        self.num_samples = num_samples if num_samples is not None else len(indices)
        self.indices = indices

        # Used for random seeds across multiple GPU instances
        self.epoch = 0
        self.seed = seed

        self.rank, self.num_replicas = get_rank_and_replica(rank=rank, num_replicas=num_replicas)

    def __iter__(self) -> Iterator[int]:
        g = torch.Generator()
        g.manual_seed(self.seed + self.epoch)  

        idx_sampler = iter(CycleRandomPermutationV2(
            self.indices,
            rank=self.rank,
            num_replicas=self.num_replicas,
            generator=g,
        ))
        for _ in range(self.num_samples):
            yield next(idx_sampler)
            

    def __len__(self) -> int:
        return self.num_samples
    
    def set_epoch(self, epoch: int) -> None:
        r"""
        Sets the epoch for this sampler. When :attr:`shuffle=True`, this ensures all replicas
        use a different random ordering for each epoch. Otherwise, the next iteration of this
        sampler will yield the same ordering.
        Args:
            epoch (int): Epoch number.
        """
        self.epoch = epoch